// require
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();

// routes
var routes = require('./routes/index');


var http = require('http');
var server = http.createServer(app);
var io = require('socket.io').listen(server);

var listOfsongs = [];

io.sockets.on('connection', function (socket) {
	
	//Gets the user's id
	var sessionId = socket.id;
	
    console.log('User connected');
    socket.emit('get all songs before me', listOfsongs);

    socket.on('songAdded',function(song){
		song.sessionId = sessionId; //Adds the user's id to the song
        listOfsongs.push(song); //Then we add it into the list of song's array
        socket.emit('my new ballSong',song); //We can now emit a event to add the ball to the scene
        socket.broadcast.emit('new songBall from other user',song); //And broadcast it to everyone
    });
	
	//When user disconnects, whe search into the array the position of his id to remove it from the song's array.
	socket.on('disconnect', function() {
		var pos = listOfsongs.map(function(e) { return e.sessionId; }).indexOf(sessionId);
		if(pos != -1) { //If sessionId is found, we delete the object from the array
			var deleted = listOfsongs.splice(pos, 1);
		}
        socket.broadcast.emit('user disconnected',sessionId);
	});

});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// routes
app.use('/', routes);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.set('port', process.env.PORT || 3000);

server.listen(app.get('port'), function(){
  console.log('Server listening on port :' + app.get('port'));
});

