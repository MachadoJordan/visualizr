var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

gulp.task('compass', function() {

	return 	gulp.src('src/scss/style.scss')
			.pipe($.compass({
				css: 'public/css',
				sass: 'src/scss'
				}))
			.pipe(gulp.dest('public/css'))
});

// compile all libs -> vendor.min.js
gulp.task('libs',function() {

	return	 gulp.src(['src/js/libs/three.min.js','src/js/libs/THREEx.WindowResize.js'])
			.pipe($.uglify())
			.pipe($.concat('vendor.min.js'))
			.pipe(gulp.dest('public/js'))
});

// compile all js -> app.js
gulp.task('scripts',function() {
	return 	gulp.src('src/js/app/*.js')
			.pipe($.concat('app.js'))
			.pipe(gulp.dest('public/js'))
});

gulp.task('watch', function () {
   	gulp.watch(['src/scss/style.scss'], ['compass']);
    gulp.watch(['src/js/app/*.js'], ['scripts']);
});

gulp.task('start',function()
{
	$.express.run({
		file : 'app.js'
	})

 	gulp.watch(['src/scss/style.scss'], ['compass']);
    gulp.watch(['src/js/app/*.js'], ['scripts']);
    gulp.watch(['src/js/libs/*.js'], ['libs']);
});

gulp.task('default', ['compass','scripts','libs', 'start']);

