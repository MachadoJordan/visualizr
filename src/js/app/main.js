var apiKey = "3cc847256318c39b41c85d4bd7d0fa8a";
var socket,sessionId,isPlaying,songLoaded,soundManager,volume;

// Global Dom element vars
var form,player,footer,time;

// Global SongBall vars 
var mySongBall,songBalls = [];
var songBallsobject = [];

// Three global vars
var scene,camera,projector,renderer;

var mousePos = {x:0,y:0};

var iHeight = window.innerHeight;
var iWidth = window.innerWidth;

window.onload = function() {

	initSoundcloud();
	initThree();
	
	//Gets elements from DOM
	form = document.querySelector("#scLink");
	player = document.querySelector('#player');
	footer = document.querySelector('footer span');
	canvas = document.querySelector('canvas');
	time = document.querySelector('#time');
	displayTime = document.querySelector('#duration');

	//Listeners
	document.addEventListener( 'mousemove', onDocumentMouseMove, false );
	canvas.addEventListener( 'mousedown', onDocumentMouseDown, false );
	form.addEventListener("submit",onSubmit,false);
	window.addEventListener("blur",focusHandler, false);
	window.addEventListener("focus", focusHandler, false);

	//Initializes SounndCloudManager
	soundManager = new SoundCloudAudioManager();

	//Initializes a new socket
	socket = io();
	
	//Adds a songBall for everyone when an user adds one
	socket.on('new songBall from other user',function(songBall)
	{
		songBall = new SongBall(songBall);
		songBall.ball.position.x = Math.random()* 1000 - 500;
		songBall.ball.position.y = Math.random()* 1000 - 500;
		songBall.ball.position.z = Math.random()* 1000 - 500;

		songBall.addScene();
		songBalls.push(songBall);
    	songBallsobject.push(songBall.ball);
		console.log('Song added from other user')
	});

	// event for getting all song after the connection 
	socket.on('get all songs before me',function(listOfSong)
	{
		for(var i=0,length = listOfSong.length;i<length;i++)
		{
			songBall = new SongBall(listOfSong[i]);
			
			songBall.ball.position.x = Math.random()* 1000 - 500;
    		songBall.ball.position.y = Math.random()* 1000 - 500;
    		songBall.ball.position.z = Math.random()* 1000 - 500;

    		songBall.addScene();
    		songBalls.push(songBall);
    		songBallsobject.push(songBall.ball);
		}
	});
	
	// my ball song
	socket.on('my new ballSong',function(song)
	{
		mySongBall = new SongBall(song);
		footer.innerHTML = song.title;
		mySongBall.ball.position.z = 500;

		mySongBall.addScene();
		songBalls.push(mySongBall);
		songBallsobject.push(mySongBall.ball);
	});

	// when user disconnect remove the ball 
	socket.on('user disconnected',function(sessionId)
	{
		var songBall = scene.getObjectByName(sessionId);
		// songBall.remove(); Doesn't work, need to understand why
		scene.remove(songBall);
	});
};
// ---------- EVENTS ---------- //

// Handles the focus on the page
function focusHandler() 
{
	if(songLoaded && isPlaying) //If a song is loaded and is playing, we stop it
	{
		isPlaying = false;
		player.pause();
	} else if(songLoaded && !isPlaying) { // Otherwise, we play it again
		isPlaying = true;
		player.play();
	}
}

// OnTimeUpdate
function onTimeUpdate()
{
	var currentTime = Math.floor(player.currentTime);
	var readableTime = readableDuration(player.currentTime);
	displayTime.innerHTML = readableTime;
	time.setAttribute("value", currentTime);
}

// OnMouseMove
function onDocumentMouseMove(event)
{
    mousePos.x = event.clientX - iWidth/2;
    mousePos.y = event.clientY - iHeight/2;
}

// OnMouseDown
function onDocumentMouseDown(event)
{
	event.preventDefault();
	var x = event.x;
    var y = event.y;
	//x -= document.querySelector('canvas').offsetLeft;
    //y -= document.querySelector('canvas').offsetTop/2;
	var vector = new THREE.Vector3((x / window.innerWidth) * 2 - 1,-(y/ window.innerHeight) * 2 + 1,0.5);
	projector.unprojectVector( vector, camera );
	var raycaster = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );

	var intersects = raycaster.intersectObjects(songBallsobject);
	if (intersects.length > 0) 
	{
		console.log('CLICK');
		//intersects[ 0 ].object.material.color.setHex( Math.random() * 0x990FFF );	
		loadSong(intersects[0].object.name);
	}
}

// OnSubmit
function onSubmit(event) 
{
	inputValue = form.elements["trackUrlInput"].value;
	if(isSoundCloudUrl(inputValue))
	{
		resolveUrl(inputValue);
	} else {
		alert('Lien incorrect.');
	}

	event.preventDefault();
}

// ---------- INIT FUNCTIONS ---------- //

// Init SoundCloudSdk
function initSoundcloud()
{
	SC.initialize({
		client_id: apiKey
	});
}

// THREE
function initThree()
{
	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera(75, iWidth/iHeight, 0.1, 2000);
	renderer = new THREE.WebGLRenderer();
	projector = new THREE.Projector();
	renderer.setSize(iWidth, iHeight);
	document.body.appendChild(renderer.domElement);
	//camera.position.z = 1000;
	camera.position.set(0, -300, 1000);
	render();
	var windowResize = THREEx.WindowResize(renderer, camera)

	console.log("--- Three initialized ---");
}

// ---------- SOUNDCLOUD FUNCTIONS ---------- //

// Tests if submitted URL is a soundcloud url
function isSoundCloudUrl(urlTrack) 
{
	var pattern = new RegExp("^https://soundcloud.com/[a-z0-9-_]+/[a-z0-9-_]+$");
    var result = pattern.test(urlTrack);

    return result;
}

//Gets track's informations
function resolveUrl(urlTrack)
{
	SC.get('/resolve', { url: urlTrack }, function(track,error) 
	{
		if(error)
		{
			//Ajouter error handler
			alert("Merci d'ajouter un lien SoundCloud correct. Erreur : " +error.message);
		}
		else
		{
			var streamUrl = track.stream_url+'?client_id=' + apiKey;
			soundManager.playStream(streamUrl);

			socket.emit("songAdded", {
				id: 			track.id,
				title: 			track.title,
				genre: 			track.genre,
				bpm: 			track.bpm,
				streamUrl: 		streamUrl
			});
		}
	});
}

// ---------- SOUND MANAGERS ---------- //

var SoundCloudAudioManager = function() 
{
    var that = this;

    var analyser;
    var audioCtx = new (window.AudioContext || window.webkitAudioContext);
    analyser = audioCtx.createAnalyser();
    analyser.fftSize = 256; // see - there is that 'fft' thing. 
    var source = audioCtx.createMediaElementSource(player); 
    source.connect(analyser);
    analyser.connect(audioCtx.destination);

    var sampleAudioStream = function() 
    {
        analyser.getByteFrequencyData(that.streamData);
        var total = 0;
        for (var i = 0; i < 80; i++) 
        { 
            total += that.streamData[i];
        }
        that.volume = total;
    };

    setInterval(sampleAudioStream, 20);
    that.volume = 0;
    that.streamData = new Uint8Array(128);
	
    this.playStream = function(streamUrl) 
    {
        player.setAttribute('src', streamUrl);
		songLoaded = true;
        isPlaying = true;
        player.play();
    };
	
	//Adds an event for when sounds completes
	player.onended = onEnded;
	player.ondurationchange = updateDuration;
	
	function onEnded() {
		songLoaded = false;
		isPlaying = false;
		footer.innerHTML = '...';
	}

	// Updates the input range's max attribute 
	function updateDuration() {
		var duration = Math.floor(player.duration);
		time.setAttribute("max", duration);
	}

	console.log('--- AudioManager initialized ---');
};

function loadSong(name)
{
	var pos = songBalls.map(function(e) { return e.sessionId; }).indexOf(name);
	if(pos != -1) {
		if(mySongBall) {
			mySongBall.ball.scale.set(1,1,1);
			mySongBall.shader.uniforms.amplitude.value = 0;
		}
		soundManager.playStream(songBalls[pos].streamUrl);
		mySongBall = songBalls[pos];
		console.log('%c LOAD SONG','background:#000000;color:#d2d2d2');
		console.log(mySongBall)
		footer.innerHTML = mySongBall.title;
	}
}

// ---------- THREEJS FUNCTIONS ---------- //

function render()
{
	if(isPlaying)
	{
		var val = 0;
		for(var i = 80; i < 128; i++) { //Treble frequencies
			val += soundManager.streamData[i]/10;
		}

		var total = 0;
		for (var i = 0; i < 80; i++) { //Bass - Middle frequencies
            total += soundManager.streamData[i];
        }
		
		volume = total/1000; //Made to get a volume in a range of 0 and 20
		
		if(mySongBall)
		{
			mySongBall.animate(val,volume);	
		}
	}

	requestAnimationFrame(render);

	// camera position
	camera.position.x += ( mousePos.x - camera.position.x ) * 0.5;
    camera.position.y += ( - mousePos.y - camera.position.y ) * 0.5;
    camera.lookAt(scene.position);

	renderer.render(scene, camera);
}

// ---------- SONGBALL ---------- //

// SongBall
function SongBall(songInfos)
{	
	this.sessionId = songInfos.sessionId;
	this.id = songInfos.id;
	this.title = songInfos.title;
	this.genre = songInfos.genre;
	this.bpm = songInfos.bpm;
	this.streamUrl = songInfos.streamUrl;


	var geometry = new THREE.SphereGeometry(60,20,20);

	// instanciate new songBallShader 
	this.shader = new SongBallShader();

	// shader material
	var shaderMaterial = new THREE.ShaderMaterial({
		attributes:     this.shader.attributes,
		uniforms:       this.shader.uniforms,
		vertexShader:   this.shader.vertexShader,
		fragmentShader: this.shader.originalEffect,
		wireframe:		true,
	});

	//glowing effect
	var shaderGlowMaterial = new THREE.ShaderMaterial({
		attributes:     this.shader.attributes,
		uniforms:       this.shader.uniforms,
		vertexShader:   this.shader.vertexShader,
		fragmentShader: this.shader.glowingEffect,
		wireframe:		false,
		blending: THREE.AdditiveBlending,
		transparent: true,
		side: THREE.FrontSide,
	});

	this.ball = new THREE.Mesh(geometry, shaderMaterial);
	this.ballGlow = new THREE.Mesh( geometry, shaderGlowMaterial);

	this.ball.name = songInfos.sessionId;
	this.ball.geometry.verticesNeedUpdate = true;
	var verts = this.ball.geometry.vertices;
	var values = this.shader.attributes.displacement.value;

	for (var v = 0; v < verts.length; v++) {
		if(v%8 == 0)
			values.push(Math.random() * 40);
		else
			values.push(0);
	}
}

// ---------- SONGBALLS'S PROTOTYPES ---------- //

SongBall.prototype.addScene = function addScene() {

	this.ballGlow.position.x = this.ball.position.x;
	this.ballGlow.position.y = this.ball.position.y;
	this.ballGlow.position.z = this.ball.position.z;


	scene.add(this.ball);
	scene.add(this.ballGlow);
	

}

SongBall.prototype.removeScene = function removeScene() {

	scene.remove(this.ball);
	scene.remove(this.ballGlow);
}


SongBall.prototype.animate = function animate(val,volume) {

	this.ball.scale.set(volume*.1,volume*.1,volume*.1);
	this.ballGlow.scale.set(volume*.2,volume*.2,volume*.2);
	this.ball.rotation.y += val*0.00003;

	if(volume>10)
	{
		this.shader.uniforms.amplitude.value = Math.abs(Math.sin(volume));	
	}
	else
	{
		this.shader.uniforms.amplitude.value = 0;
	}

	if(volume>15)
	{
		var verts = this.ball.geometry.vertices;
		var values = this.shader.attributes.displacement.value;

		for (var v = 0; v < verts.length; v++) {
			values[v] += val;
		}
	}
}

// ---------- OTHER FUNCTIONS ---------- //

function readableDuration(seconds) 
{
    sec = Math.floor( seconds );    
    min = Math.floor( sec / 60 );
    min = min >= 10 ? min : '0' + min;    
    sec = Math.floor( sec % 60 );
    sec = sec >= 10 ? sec : '0' + sec;    
    return min + ':' + sec;
}