//https://raw.githubusercontent.com/mrdoob/three.js/master/examples/js/shaders/MirrorShader.js

function SongBallShader ()
{
	this.attributes = {
		displacement: {
			type: 'f', // a float
			value: [] // an empty array
		}
	},
	this.uniforms = {
		amplitude: {
			type: 'f', // a float
			value: 0
		},
		glowColor: {
		 	type: "c",
		 	value: new THREE.Color("rgb(102,51,204)") 
		}
	},
	this.vertexShader = [
		"uniform float amplitude;",
		"attribute float displacement;",
		"varying vec3 vNormal;",
		"varying float displacementColor;",
		"varying float intensity;",

		"void main() {",
		"vec3 vNormal = normalize( normalMatrix * normal );",
		"intensity = pow( 0.3 - dot( vNormal, vec3( 0.0, 0.0, 1.0 ) ), 2.3 );",
		"displacementColor = amplitude * displacement;",
		"vec3 newPosition = position + normal * vec3(displacement*amplitude);",
		"gl_Position = projectionMatrix * modelViewMatrix * vec4(newPosition, 1.0);",
		"}"
	].join("\n"),
	this.originalEffect = [
		
		"varying float displacementColor;",
		"varying float intensity;",

		"void main() {",
		"gl_FragColor = vec4(0,displacementColor,1,1.0);",
		"}"
	].join("\n")
	this.glowingEffect = [
		
		"varying float displacementColor;",
		"varying float intensity;",
		"varying vec3 vNormal;",
		"uniform vec3 glowColor;",

		"void main() {",
		"vec3 glow = glowColor * intensity;",
		"gl_FragColor = vec4(glow,1.0);",
		"}"
	].join("\n")
}